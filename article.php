<?php 
    session_start();
    require 'bdd/bddconfig.php';

    $idArticle = 0;
    $idok = isset($_GET['idArticle']);
    if($idok == true) {
        $idArticle = intval(htmlspecialchars($_GET['idArticle']));
    }else{
        /*Redirige vers la page d'accueil*/
        $server = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])),'/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");    
    }

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );
    //liste des articles
    $listeArticles = $objBdd->prepare("SELECT * FROM article WHERE idArticle = :id");
    $listeArticles->bindParam(':id', $idArticle, PDO::PARAM_INT);
    $listeArticles->execute();
    $article = $listeArticles->fetch();

    //récupérer les liens de l'article
$listeLiens = $objBdd->prepare("SELECT * FROM article,document WHERE idDoc=:id AND type='lien'");
    $listeLiens->bindParam(':id', $idArticle, PDO::PARAM_INT);
    $listeLiens->execute();
    $liens = $listeLiens->fetch();
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

$titre = $article['titre'];
//Début de l'affichage (mise en tampon)
ob_start();
?>
            <h2 style="padding:10px;"><?php echo nl2br($article['titre']); ?></h2>
            <span style="padding:10px; text-align:justify;"><?php echo nl2br($article['texte']); ?></span>

            <span style="margin:35px;">Plus d'infos
            <a href="<?php echo $liens['url']; ?>">ici.</a>

<?php 
$contenu = ob_get_clean();
require 'gabarit/template.php';
?>