<?php ob_start();
session_start();
require "bdd/bddconfig.php";
$titre = "accueil";
    try {
        $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname; charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
        $listeArticles = $objBdd->prepare("SELECT * FROM article, user WHERE article.idUser = user.idUser ORDER BY datepub DESC LIMIT 5");
        $listeArticles->bindParam(':id', $idTheme, PDO::PARAM_INT);
        $listeArticles->execute();
    } catch (Exception $prmE) { die('Erreur : ' . $prmE->getMessage()); }
 ?>


    <h1>Base de connaissance IT</h1>
    <p></p>

    <div>
        <div>
            <h6>Les 5 derniers articles publiés</h6>
        </div>
        <div><ul>
        <?php foreach ($listeArticles as $article) {?>
                <li> <a href="article.php?idArticle=<?=$article['idArticle']; ?>">
                <span><?= $article['titre']; ?> <?php echo " (" . $article['pseudo'] . " - " . $article['datepub'] . ")"; ?></span></a></li>
        <?php } ?>
        
        </ul>
        </div>
    </div>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>