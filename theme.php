<?php 
    session_start();
    require 'bdd/bddconfig.php';

    $idTheme = 0;
    if(isset($_GET['idTheme'])) {
        $idTheme = intval(htmlspecialchars($_GET['idTheme']));
    }
    
    /*else{
        //Redirige vers la page d'accueil
        $server = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])),'/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");    
    }*/

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $listeArticles = $objBdd->prepare("SELECT * FROM article,user WHERE idTheme = :id AND article.idUser = user.idUser");
    $listeArticles->bindParam(':id',$idTheme, PDO::PARAM_INT);
    $listeArticles->execute();
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>

<?php
$titre = "Thème";
//Début de l'affichage (mise en tampon)
ob_start();
?>
    
    <ol>
            <?php foreach ($listeArticles as $article) { ?>
                <li><a href="article.php?idArticle=<?= $article['idArticle']; ?>">
                    <span><h2><?php echo $article['titre']." <small>(par ".$article['pseudo'].")</small>"; ?></h2></span>
            </a></li>
            <?php } ?>
    </ol>
    
<?php 
$contenu = ob_get_clean();
require 'gabarit/template.php';
?>