<?php 
require "bdd/bddconfig.php";
/*session_start();*/
try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname; charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $listeThemes = $objBdd->prepare("SELECT * FROM theme");
    $listeThemes->execute();
           
} catch (Exception $prmE) { die('Erreur : ' . $prmE->getMessage()); }
    ob_start(); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $titre ?></title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <header>
        <h1 id="baseit">BaseIT</h1>
        <h1>Bases de connaissance IT</h1>
        <div class="login"><?php 
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié
                    echo 'Salut à toi, '.$_SESSION['logged_in']['pseudo'].'! ';
                    //affichage "se déconnecter"(logout.php), "prif", "paramètres", etc.
                    ?><br><br><a href="logout.php">Se déconnecter</a><?php
                }else{
                    //Personne n'est authentifié
                    //affichage d'un lien pour se connecter
                    ?>
                    <a href="login.php">Connexion</a>
                    <?php
                }
                
                ?>
        </div>
    </header>
    <div class="div1">
        <aside>
            <ul>
                <li id="listethemes">Thèmes</li>

                <?php foreach ($listeThemes as $theme) { ?> 
                <li><a href="theme.php?idTheme=<?=$theme['idTheme']; ?>">
                    <i></i>
                    <span><?= $theme['nom']; ?></span></a>
                </li>
                <?php } ?>
                
                <?php 
                    foreach ($listeThemes as $theme) {
                        echo $theme['nom'];
                    }
                ?>
                
            </ul>
        </aside>
        <section>
        <?php echo $contenu ; ?>        
        </section>
    </div>
</body>
</html>