<link rel="stylesheet" href="style.css">
<?php $titre = "login_action"; ?>
<?php ob_start(); ?>

<?php //activer les sessions
session_start();

//Tester si les variables POST existent
$paramOK = false;
if (isset($_POST["login"])) {
    $login = strtolower(htmlspecialchars($_POST["login"]));
    if (isset($_POST["password"])) {
        $password = htmlspecialchars($_POST["password"]);
        $paramOK = true;
    }
}

//si login et password sont bien reçus
if ($paramOK == true) {
    //vérifier si le login passord est correct (base de données) : voir après
} else {
    die('Vous devez fournir un login et un mot de passe');
}
require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $PDOlistlogins = $objBdd->prepare("SELECT * FROM user WHERE login = :login ");
    $PDOlistlogins->bindParam(':login', $login, PDO::PARAM_STR);
    $PDOlistlogins->execute();
    //S'il y a un résultat à la requête 
    $row_userweb = $PDOlistlogins->fetch();
    if ($row_userweb != false) {
        //il existe un login identique dans la base
        // vérif du password : voir après
        //vérifier si les hash des password correspondent
        if (password_verify($password, $row_userweb['password'])) {
            //authentification réussie
            //tableau des données à stocker en session
            $session_data = array(
                'id' => $row_userweb['id'],
                'login' => $row_userweb['login'],
                'pseudo' => $row_userweb['pseudo']
            );
            //régénérer le session id
            session_regenerate_id();
            //enregistrer les données dans une variable de session
            $_SESSION['logged_in'] = $session_data;
            $PDOlistlogins->closeCursor();

            /* Redirige vers la page d'accueil */
            $serveur = $_SERVER['HTTP_HOST'];
            $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
            $page = 'index.php';
            header("Location: http://$serveur$chemin/$page");


            //création de la variable de session : voir après
        } else {
            //Mauvais password
            session_destroy();
            die('Authentification incorrecte');
        }
    } else {
        //Mauvais login
        session_destroy();
        die('Authentification incorrecte');
    }
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

?>
<?php $contenu = ob_get_clean() ?>
<?php require 'gabarit/template.php' ?>